package com.practitioner.bbva.controllers;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.practitioner.bbva.models.Empleado;
import com.practitioner.bbva.models.Producto;
import com.practitioner.bbva.models.Venta;
import com.practitioner.bbva.service.IEmpleadoService;
import com.practitioner.bbva.service.IVentaService;

@RestController
@RequestMapping("/rest/ventas")
public class VentaController {
	
	@Autowired
	IVentaService ventaService;
	
	@Autowired
	IEmpleadoService empleadoService;
	
	@GetMapping("/listar")
	public CollectionModel<EntityModel<Venta>> listar() {

		List<Venta> lista = ventaService.findAll();

		return CollectionModel.of(lista.stream()
				.map(fact -> EntityModel.of(fact))
				.collect(Collectors.toUnmodifiableList()))
				.add(linkTo(methodOn(this.getClass()).listar()).withSelfRel());

	}

	
	@GetMapping("/{numeroFactura}")
	public ResponseEntity<?> getVenta(@PathVariable(name = "numeroFactura") String numeroFactura) {
		
		Venta v = ventaService.getVenta(numeroFactura);
		
		if(v==null) {
			return new ResponseEntity<>("Venta no encontrada", HttpStatus.NOT_FOUND);
		}
		
		return ResponseEntity.ok(EntityModel.of(v).add(
				Arrays.asList(
						linkTo(methodOn(this.getClass()).getVenta(v.getNumeroFactura())).withSelfRel(),
						linkTo(methodOn(this.getClass()).getProductos(v.getNumeroFactura())).withRel("Productos").withTitle("Lista de productos de la venta")
				)
				));

	}
	
	
	@GetMapping("/{numeroFactura}/productos")
	public ResponseEntity<?> getProductos(@PathVariable(name = "numeroFactura") String numeroFactura) {
		
		Venta v = ventaService.getVenta(numeroFactura);
		
		if(v==null) {
			return new ResponseEntity<>("Venta no encontrada", HttpStatus.NOT_FOUND);
		}
		
		

		return ResponseEntity.ok(v.getListaProductos());

	}
	
	@PostMapping
	public ResponseEntity<?> addVenta(@RequestBody Venta venta) {
		
		if(venta == null || venta.getIdEmpleado() ==null || venta.getListaProductos()== null || venta.getListaProductos().isEmpty()) {
			 throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		}
		Empleado e = empleadoService.getEmpleado(venta.getIdEmpleado());
		if(e==null) {
			return  new ResponseEntity<>("Empleado no encontrado", HttpStatus.NOT_FOUND);
		}

		if(venta.getListaProductos().isEmpty())
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

		ventaService.addVenta(venta);
		
		return ResponseEntity
				.created(linkTo(methodOn(this.getClass()).getVenta(venta.getNumeroFactura())).withSelfRel().toUri())
				.body("Venta creado con éxito");
		
	}
	
	@PutMapping("/{numeroFactura}")
	public ResponseEntity<?> addProductos(@PathVariable(name = "numeroFactura") String numeroFactura, @RequestBody List<Producto> listaProductos) {
		
		Venta v = ventaService.getVenta(numeroFactura);
		
		if(v==null) {
			return new ResponseEntity<>("Venta no encontrada", HttpStatus.NOT_FOUND);
		}
		v.setListaProductos(Stream.concat(v.getListaProductos().stream(), listaProductos.stream()).collect(Collectors.toList()));
		ventaService.updateVenta(v);
		return ResponseEntity.ok(EntityModel.of(v).add(
				Arrays.asList(
						linkTo(methodOn(this.getClass()).getVenta(v.getNumeroFactura())).withSelfRel(),
						linkTo(methodOn(this.getClass()).getProductos(v.getNumeroFactura())).withRel("Productos").withTitle("Lista de productos de la venta")
				)
				));

	}
	
	@PutMapping("/{numeroFactura}/{idProducto}")
	public ResponseEntity<?> removerProducto(@PathVariable(name = "numeroFactura") String numeroFactura, @PathVariable(name = "idProducto") String idProducto) {
		
		Venta v = ventaService.getVenta(numeroFactura);
		
		if(v==null) {
			return new ResponseEntity<>("Venta no encontrada", HttpStatus.NOT_FOUND);
		}
		
		ventaService.removerProducto(v.getId(), idProducto);
		
		return ResponseEntity.ok().build();

	}


}
