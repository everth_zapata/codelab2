package com.practitioner.bbva.dao;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.practitioner.bbva.models.Venta;


@Repository
public interface IVentaDao extends MongoRepository<Venta, String>,
RepositorioVentaPersonalizado {
	
	public Venta findByNumeroFactura(String numeroFactura);
	public List<Venta> findByIdEmpleado(String idEmpleado);
	
}
