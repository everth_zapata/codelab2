package com.practitioner.bbva.service;

import java.util.List;

import com.practitioner.bbva.models.Empleado;

public interface IEmpleadoService {
	
	public List<Empleado> findAll();
	
	public String addEmpleado(Empleado empleado);

	public Empleado getEmpleado(String id);

	public void updateEmpleado(String id, Empleado empleado);
	
	public void deleteEmpleado(String id);

}
