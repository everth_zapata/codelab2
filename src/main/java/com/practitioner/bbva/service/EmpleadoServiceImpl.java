package com.practitioner.bbva.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import com.practitioner.bbva.dao.IEmpleadoDao;
import com.practitioner.bbva.models.Empleado;

@Service
public class EmpleadoServiceImpl implements IEmpleadoService{
	
	@Autowired
	IEmpleadoDao empleadoDao;

	@Override
	public List<Empleado> findAll() {
		
		return empleadoDao.findAll();
	}

	@Override
	public String addEmpleado(Empleado empleado) {
		// TODO Auto-generated method stub
		try {
			
			empleadoDao.insert(empleado);
			
			return empleado.getId();
			
		}catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
		}
		
		
	}

	@Override
	public Empleado getEmpleado(String id) {
		
		final Optional<Empleado> p = empleadoDao.findById(id);
        return p.isPresent() ? p.get() : null;
	}

	@Override
	public void updateEmpleado(String id, Empleado empleado) {
		empleado.setId(id);
		empleadoDao.save(empleado);
		
	}

	@Override
	public void deleteEmpleado(String id) {
		// TODO Auto-generated method stub
		empleadoDao.deleteById(id);
	}

	


}
