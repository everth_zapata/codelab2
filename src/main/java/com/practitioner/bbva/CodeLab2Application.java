package com.practitioner.bbva;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class CodeLab2Application{

	
	public static void main(String[] args) {
		SpringApplication.run(CodeLab2Application.class, args);
	}
	
}
