package com.practitioner.bbva.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import com.practitioner.bbva.dao.IVentaDao;
import com.practitioner.bbva.models.Venta;

@Service
public class VentaServiceImpl implements IVentaService{

	@Autowired
	IVentaDao ventaDao;
	
	@Override
	public List<Venta> findAll() {
		// TODO Auto-generated method stub
		return ventaDao.findAll();
	}

	@Override
	public String addVenta(Venta venta) {
		// TODO Auto-generated method stub
		try {
			Long cont = ventaDao.count()+1;
			venta.setNumeroFactura(String.format("%05d", cont));
			venta.setFecha(new Date());
			ventaDao.insert(venta);
			return venta.getNumeroFactura();
		
		}catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
		}
	}

	@Override
	public Venta getVenta(String numeroFactura) {
		
		final Optional<Venta> p = Optional.ofNullable(ventaDao.findByNumeroFactura(numeroFactura));
        return p.isPresent() ? p.get() : null;
	}

	@Override
	public List<Venta> ventasXEmpleado(String id) {
		return ventaDao.findByIdEmpleado(id);
	}

	@Override
	public void updateVenta(Venta venta) { 
		ventaDao.save(venta);
	}

	@Override
	public void removerProducto(String id, String idProducto) {
		ventaDao.removerProducto(id, idProducto);
		
	}

}
