package com.practitioner.bbva.dao;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.practitioner.bbva.models.Empleado;

@Repository
public interface IEmpleadoDao extends MongoRepository<Empleado, String>{

}
