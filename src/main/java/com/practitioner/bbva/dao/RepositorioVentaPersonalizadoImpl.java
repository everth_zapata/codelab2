package com.practitioner.bbva.dao;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import com.practitioner.bbva.models.Venta;

public class RepositorioVentaPersonalizadoImpl implements RepositorioVentaPersonalizado{
	
	@Autowired
	MongoTemplate mongoTemplate;

	@Override
	public void removerProducto(String id, String idProducto) {

		Query filtro = new Query();
        filtro.addCriteria(Criteria.where("_id").is(new ObjectId(id)));
		this.mongoTemplate.updateMulti(filtro,
		        new Update().pull("listaProductos", Query.query(Criteria.where("_id").is(idProducto))), Venta.class);
		
	}



}
