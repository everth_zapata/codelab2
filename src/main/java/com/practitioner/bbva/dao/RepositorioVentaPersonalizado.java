package com.practitioner.bbva.dao;


import org.springframework.stereotype.Repository;


@Repository
public interface RepositorioVentaPersonalizado {
	
    public void removerProducto(String id, String idProducto);

}
